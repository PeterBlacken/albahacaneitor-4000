
#define pump 3
#define level A3
#define hsensor A1

int niveldeAgua = 0;
int refHumedad = 500; // Valor de humedad fijado mediante processing
void setup()
{
  Serial.begin(9600);
  //Serial.println("hola mundo");
  pinMode(pump, OUTPUT);
  pinMode(level, INPUT_PULLUP);
  pinMode(hsensor, INPUT);
}
void loop()
{
  int niveldeAgua = digitalRead(level);
  int humedad = analogRead(hsensor);
  if(Serial.available())//if data available
  { 
    int refHumedad = Serial.read();
    if ((niveldeAgua==1)&&(humedad<refHumedad))
    {
      Serial.println(humedad);
      digitalWrite(pump, HIGH);
    }else 
    {

      digitalWrite(pump, LOW);
    }

  }    
}
